from google.appengine.ext import ndb
from google.appengine.api import images
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
import time


class User(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    created_date = ndb.DateProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    email = ndb.StringProperty()
    password = ndb.StringProperty()
    fb_id = ndb.StringProperty()
    fb_access_token = ndb.StringProperty()
    fb_username = ndb.StringProperty()
    name = ndb.StringProperty()
    address = ndb.StringProperty()
    about_me = ndb.StringProperty()
    photo = ndb.StringProperty()
    role = ndb.StringProperty()
    # profile fields
    first_name = ndb.StringProperty()
    middle_name = ndb.StringProperty()
    last_name = ndb.StringProperty()
    stamps = ndb.StringProperty(repeated=True, default=None)

    # details of school
    school_documents = ndb.KeyProperty(kind="SchoolDocuments")
    school_name = ndb.StringProperty()
    school_address = ndb.StringProperty()
    school_contact = ndb.StringProperty(repeated=True) # <-- json so we can, {'Richmond Wang': '+639236686351'}
    school_course = ndb.StringProperty()
    school_year_level = ndb.IntegerProperty(default=1)

    campaigns = ndb.KeyProperty(kind="Campaign", repeated=True)

    def to_object(self):
        details = {}
        details["created"] = int(time.mktime(self.created.timetuple()))
        details["updated"] = int(time.mktime(self.updated.timetuple()))
        details["key"] = self.key.urlsafe()
        details["email"] = self.email
        details["fb_id"] = self.fb_id
        details["name"] = self.name
        details["address"] = self.address
        details["about_me"] = self.about_me
        details["first_name"] = self.first_name
        details["middle_name"] = self.middle_name
        details["last_name"] = self.last_name
        details["photo"] = self.photo
        details["id"] = self.key.id()

        if self.photo:
            if self.fb_id:
                details["photo"] = self.photo
            else:
                image = blobstore.BlobInfo.get(self.photo)
                details["photo"] = images.get_serving_url(image)
        details["role"] = self.role
        details['stamps'] = []
        if self.stamps:
            for stamp in self.stamps:
                details['stamps'].append(stamp)
        return details


class Campaign(ndb.Model):
    # more details about the funding
    funding_goal = ndb.IntegerProperty(default=0)
    video_url = ndb.StringProperty()
    photo = ndb.StringProperty(repeated=True, default=None)
    duration_days = ndb.IntegerProperty(default=0)
    duration_date = ndb.DateTimeProperty()
    message = ndb.TextProperty()
    reward = ndb.TextProperty()
    email = ndb.StringProperty()

    def to_object(self):
        details = {}
        details["funding_goal"] = self.funding_goal
        details["video_url"] = self.video_url
        details["duration_date"] = self.duration_date
        details["duration_days"] = self.duration_days
        details["message"] = self.message
        details["id"] = self.key.id()
        details["email"] = self.email

        return details


class SchoolDocuments(ndb.Model):
    file_name = ndb.StringProperty()
    file_key = ndb.StringProperty()
    file_type = ndb.StringProperty()
    uploaded = ndb.DateTimeProperty(auto_now_add=True)

    def to_objects(self):
        details = {}
        details["file_name"] = self.file_name
        details["uploaded"] = self.uploaded


class PasswordResetToken(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    email = ndb.StringProperty()
    token = ndb.StringProperty()
    expires = ndb.DateTimeProperty()
