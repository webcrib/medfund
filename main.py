import webapp2, jinja2, os
from webapp2_extras import routes
from models import User, PasswordResetToken, Campaign
from functions import *
import json as simplejson
import logging
import urllib
import time
import uuid
import datetime
import hashlib
import base64
import facebook

from google.appengine.ext import ndb
from google.appengine.api import urlfetch

from settings import SETTINGS
from settings import SECRET_SETTINGS
from settings import ACCOUNT_TYPES

jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)), autoescape=True)


def login_required(fn):
    '''So we can decorate any RequestHandler with #@login_required'''
    def wrapper(self, *args):
        if not self.user:
            self.redirect(self.uri_for('www-login', referred=self.request.path))
        else:
            return fn(self, *args)
    return wrapper


def admin_required(fn):
    '''So we can decorate any RequestHandler with @admin_required'''
    def wrapper(self, *args):
        if not self.user:
            self.redirect(self.uri_for('www-login'))
        elif self.user.admin:
            return fn(self, *args)
        else:
            self.redirect(self.uri_for('www-front'))
    return wrapper


def hash_password(email, password):
    i = email + password + SECRET_SETTINGS["password_salt"]
    return base64.b64encode(hashlib.sha1(i).digest())


"""Request Handlers Start Here"""



class BaseHandler(webapp2.RequestHandler):
    def __init__(self, request=None, response=None):
        self.now = datetime.datetime.now()
        self.tv = {}
        self.settings = SETTINGS.copy()
        self.initialize(request, response)
        self.has_pass = False
        self.tv["version"] = os.environ['CURRENT_VERSION_ID']
        self.local = False
        if "127.0.0.1" in self.request.uri or "localhost" in self.request.uri:
            self.local = True
        # misc
        self.tv["current_url"] = self.request.uri
        self.tv["fb_login_url"] = facebook.generate_login_url(self.request.path, self.uri_for('www-fblogin'))

        if "?" in self.request.uri:
            self.tv["current_base_url"] = self.request.uri[0:(self.request.uri.find('?'))]
        else:
            self.tv["current_base_url"] = self.request.uri

        try:
            self.tv["safe_current_base_url"] = urllib.quote(self.tv["current_base_url"])
        except:
            logging.exception("safe url error")

        self.tv["request_method"] = self.request.method

        self.session = self.get_session()
        self.user = self.get_current_user()


    def render(self, template_path=None, force=False):
        self.tv["current_timestamp"] = time.mktime(self.now.timetuple())
        self.settings["current_year"] = self.now.year
        self.tv["settings"] = self.settings

        if self.request.get('error'):
            self.tv["error"] = self.request.get("error").strip()
        if self.request.get('success'):
            self.tv["success"] = self.request.get("success").strip()
        if self.request.get('warning'):
            self.tv["warning"] = self.request.get("warning").strip()

        if self.user:
            self.tv["user"] = self.user.to_object()

        if self.request.get('json') or not template_path:
            self.response.out.write(simplejson.dumps(self.tv))
            return

        template = jinja_environment.get_template(template_path)
        self.response.out.write(template.render(self.tv))
        logging.debug(self.tv)


    def get_session(self):
        from gaesessions import get_current_session
        return get_current_session()


    def get_current_user(self):
        if self.session.has_key("user"):
            user = User.get_by_id(self.session["user"])
            return user
        else:
            return None


    def login(self, user):
        self.session["user"] = user.key.id()
        return

    def login_fb(self, fb_content, access_token):
        self.logout()
        user = User.query(User.fb_id == fb_content["id"]).get()
        if not user:
            email = fb_content["email"]
            if email:
                user = User.query(User.email == email).get()

            if user:
                # Merge User

                user.fb_id = fb_content["id"]
                try:
                    user.fb_username = fb_content["username"]
                except:
                    logging.exception("no username?")
                user.first_name = fb_content["first_name"]
                try:
                    user.last_name = fb_content["last_name"]
                except:
                    logging.exception("no last_name?")
                try:
                    user.middle_name = fb_content["middle_name"]
                except:
                    logging.exception('no middle name?')

                user.name = user.first_name
                if user.middle_name:
                    user.name += " " + user.middle_name

                if user.last_name:
                    user.name += " " + user.last_name

                try:
                    user.fb_access_token = access_token
                except:
                    logging.exception('no access token')
            else:
                user = User()
                user.fb_id = fb_content["id"]
                try:
                    user.fb_username = fb_content["username"]
                except:
                    logging.exception("no username?")
                user.email = fb_content["email"]
                user.first_name = fb_content["first_name"]
                try:
                    user.last_name = fb_content["last_name"]
                except:
                    logging.exception("no last_name?")
                try:
                    user.middle_name = fb_content["middle_name"]
                except:
                    logging.exception('no middle name?')

                user.name = user.first_name
                if user.middle_name:
                    user.name += " " + user.middle_name

                if user.last_name:
                    user.name += " " + user.last_name

                try:
                    user.fb_access_token = access_token
                except:
                    logging.exception('no access token')

            user.put()
        self.login(user)
        return


    def logout(self):
        if self.session.is_active():
            self.session.terminate()
            return


    def iptolocation(self):
        country = self.request.headers.get('X-AppEngine-Country')
        logging.info("COUNTRY: " + str(country))
        if country == "GB":
            country = "UK"
        if country == "ZZ":
            country = ""
        if country is None:
            country = ""
        return country



class FrontPage(BaseHandler):
    def get(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard'))
            return

        self.tv["current_page"] = "FRONT"
        self.render('frontend/front.html')


class RegisterPage(BaseHandler):
    def get(self):
        if self.request.get('redirect'):
            self.tv['redirect'] = self.request.get('redirect')

        if self.request.get('type'):
            account_type = self.request.get('type').upper()
            if account_type in ACCOUNT_TYPES:
                self.tv['role'] = account_type

        if self.user:
            self.redirect(self.uri_for('www-dashboard', referred="register"))
            return

        self.tv["current_page"] = "REGISTER"
        self.render('frontend/register.html')


    def post(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard'))
            return

        if self.request.get('first_name') and self.request.get('last_name') and self.request.get('email') and self.request.get('password') and self.request.get('confirm_password'):
            if self.request.get('password') != self.request.get('confirm_password'):
                self.redirect(self.uri_for('www-register', error = "Passwords do not match."))
                return

            email = self.request.get('email').strip().lower()
            first_name = self.request.get('first_name').strip().lower().title()
            last_name = self.request.get('last_name').strip().lower().title()
            password = self.request.get('password')

            user = User.get_by_id(email)
            if user:
                self.redirect(self.uri_for('www-login', error = "User already exists. Please log in."))
                return

            user = user = User(id=email)
            user.password = hash_password(email, password)
            user.email = email
            user.name = first_name+ ' ' +last_name
            if self.request.get('role'):
                user.role = self.request.get('role')
            else:
                user.role = 'STUDENT'
            user.put()
            self.login(user)
            if self.request.get('redirect'):
                self.redirect(str(self.request.get('redirect')))
            else:
                self.redirect(self.uri_for('www-dashboard'))
            return
        else:
            self.redirect(self.uri_for('www-register', error = "Please enter all the information required."))


class Logout(BaseHandler):
    def get(self):
        if self.user:
            self.logout()
        self.redirect(self.uri_for('www-login', referred="logout"))


class LoginPage(BaseHandler):
    def get(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard', referred="login"))
            return

        if self.request.get('email'):
            self.tv["email"] = self.request.get("email").strip()

        self.tv["current_page"] = "LOGIN"
        self.render('frontend/login.html')


    def post(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard'))
            return

        if self.request.get('email') and self.request.get('password'):
            email = self.request.get('email').strip().lower()
            password = self.request.get('password')
            user = User.get_by_id(email)
            if not user:
                self.redirect(self.uri_for('www-login', error="User not found. Please try another email or register."))
                return
            if user.password == hash_password(email, password):
                self.login(user)
                if self.request.get('goto'):
                    self.redirect(self.request.get('goto'))
                else:
                    self.redirect(self.uri_for('www-dashboard'))
                return
            else:
                self.redirect(self.uri_for('www-login', error="Wrong password. Please try again.", email=email))
                return
        else:
            self.redirect(self.uri_for('www-login', error="Please enter your email and password."))


class FBLoginPage(BaseHandler):
    def get(self):
        if not self.settings["enable_fb_login"]:
            self.redirect(self.uri_for("www-login"))
            return

        if self.user:
            self.redirect(self.uri_for('www-dashboard', referred="fblogin"))
            return

        if self.request.get('code') and self.request.get('state'):
            state = self.request.get('state')
            code = self.request.get('code')
            access_token = facebook.code_to_access_token(code, self.uri_for('www-fblogin'))
            if not access_token:
                # Assume expiration, just redirect to login page
                self.redirect(self.uri_for('www-login', referred="fblogin", error="We were not able to connect with Facebook. Please try again."))
                return

            url = "https://graph.facebook.com/me?access_token=" + access_token

            result = urlfetch.fetch(url)
            if result.status_code == 200:
                self.login_fb(simplejson.loads(result.content), access_token)
                self.redirect(str(state))
                return

        else:
            self.redirect(facebook.generate_login_url(self.request.get('goto'), self.uri_for('www-fblogin')))


class DashboardPage(BaseHandler):
    #@login_required
    def get(self):
        self.tv["current_page"] = "DASHBOARD"
        self.render('frontend/dashboard.html')


class ForgotPassHandler(BaseHandler):
    def get(self):
        self.tv["current_page"] = "FORGOT PASSWORD"
        self.render('frontend/forgot-pass.html')

    def post(self):
        if self.request.get('email'):
            self.tv["user"] = User.query(User.email == self.request.get("email")).get()
            if self.tv["user"]:
                token = str(uuid.uuid4())
                reset_token = PasswordResetToken(id = token)
                reset_token.email = self.tv["user"].email
                reset_token.token = token
                reset_token.expires = datetime.datetime.now() + datetime.timedelta(hours = 1)
                reset_token.put()

                send_reset_password_email(self.tv["user"], token)
                self.redirect('/forgot-pass?success='+ urllib.quote('Details about how to reset your password have been sent to you by email.'))
            else:
                self.redirect('/forgot-pass?error='+ urllib.quote('Invalid Email.'))
            return


class ProfilePage(BaseHandler):
    def get(self, *args, **kwargs):
        if kwargs['profile_id']:
            if self.user:
                if self.user.key.urlsafe() == kwargs['profile_id']:
                    self.tv['owner'] = True
                    self.tv['profile_owner'] = ndb.Key(urlsafe=kwargs['profile_id']).get().to_object()
                else:
                    self.tv['owner'] = False
                    self.tv['profile_owner'] = ndb.Key(urlsafe=kwargs['profile_id']).get().to_object()
            else:
                self.tv['redirect'] = self.request.url
                self.tv['profile_owner'] = ndb.Key(urlsafe=kwargs['profile_id']).get().to_object()
        self.render('frontend/profile.html')

    def post(self, *args, **kwargs):
        if self.request.get('address') and self.request.get('about_me'):
            user = self.user.key.get()
            user.address = self.request.get('address').strip()
            user.about_me = self.request.get('about_me').strip()
            user.put()
            self.redirect(self.request.url + '?success=Changes has been saved.')
        else:
            self.redirect(self.request.url + '?error=Fill all fields')

class MyProfilePage(BaseHandler):
    def get(self, *args, **kwargs):
        if kwargs['profile_id']:
            if self.user:
                if self.user.key.urlsafe() == kwargs['profile_id']:
                    self.tv['owner'] = True
                    self.tv['profile_owner'] = ndb.Key(urlsafe=kwargs['profile_id']).get().to_object()
                else:
                    self.tv['owner'] = False
                    self.tv['profile_owner'] = ndb.Key(urlsafe=kwargs['profile_id']).get().to_object()
            else:
                self.tv['redirect'] = self.request.url
                self.tv['profile_owner'] = ndb.Key(urlsafe=kwargs['profile_id']).get().to_object()

        campaigns = Campaign.query().fetch(100)
        self.tv['description'] = []
        for camp in campaigns:
            camps = camp.to_object()
            self.tv['description'].append(camps)


        self.render('frontend/my_profile.html')

class CreateCampaignPage(BaseHandler):
    @login_required
    def get(self):
        #self.tv["current_page"] = "Campaign"

        self.render('frontend/create_campaign.html')

    def post(self):
        if self.request.get('fund'):
            logging.critical('fund')

            campaign = Campaign()
            campaign.email = self.request.get('userid')
            campaign.funding_goal = int(self.request.get('fund'))
            campaign.video_url = self.request.get('urllink')
            if self.request.get('days'):
                campaign.duration_days = int(self.request.get('days'))
            elif self.request.get('setdate'):
                campaign.duration_date = datetime.datetime.strptime(self.request.get('setdate'), '%Y-%m-%d %H:%M:%S')

            campaign.message = self.request.get('thoughts')
            campaign.reward = self.request.get('rewards')
            campaign.put()

        self.render('frontend/create_campaign.html')

class ListCampaignPage(BaseHandler):
    def get(self):
        users = User.query(User.role=="STUDENT").fetch(100)
        self.tv['userlist'] = []
        for student in users:
            self.tv['userlist'].append(student.to_object())

        # retrieving all VIDEOS ---
        campaigns = Campaign.query().fetch(100)
        self.tv['description'] = []
        for camp in campaigns:
            camps = camp.to_object()
            self.tv['description'].append(camps)

        self.render('frontend/campaign_list.html')

class PasswordReset(BaseHandler):
    def get(self):
        if self.request.get("token"):
            r = PasswordResetToken.get_by_id(self.request.get("token"))

            if r:
                user = User.get_by_id(r.email)
                self.tv["current_user"] = user.to_object()
                self.tv["token"] = self.request.get("token")
                self.render("frontend/reset-password.html")
            else:
                self.redirect('/password/reset?error='+ urllib.quote('Invalid Token.'))
        else:
            self.render("frontend/reset-password.html")

    def post(self):
        if self.request.get("password_original") == self.request.get("password_retype"):
            if self.request.get("email"):
                user = User.get_by_id(self.request.get("email"))
                user.password = hash_password(self.request.get("email"), self.request.get("password_original"))
                user.put()
                self.login(user)

                r = PasswordResetToken.get_by_id(self.request.get("token"))
                r.key.delete()

                self.redirect(self.uri_for('www-dashboard'))
        else:
            self.redirect('/password/reset?error='+ urllib.quote('Password does not match!.'))
            return

class CampaignPage(BaseHandler):
    def get(self):
        self.render('frontend/campaign.html')


class ProfilePhotoUploadPage(BaseHandler):
    def get(self):
        self.tv["current_page"] = "ERROR"
        self.render('frontend/dynamic404.html')

    def post(self):
        # if not photo:
        #     self.redirect(str(self.request.get('redirect') + "?error=" + urllib.quote("Please choose a photo to upload")))
        #     return
        if upload_photo(self.user.key.id(), self.request.POST.get('files')):
            self.redirect(str(self.request.get('redirect')))
        else:
            self.redirect(str(self.request.get('redirect') + "?error=" + urllib.quote("Upload did not succeed. Please try it again.")))


class ErrorHandler(BaseHandler):
    def get(self, page):
        self.tv["current_page"] = "ERROR"
        self.render('frontend/dynamic404.html')


site_domain = SETTINGS["site_domain"].replace(".","\.")

app = webapp2.WSGIApplication([
    routes.DomainRoute(r'<:' + site_domain + '|localhost|' + SETTINGS["app_id"] + '\.appspot\.com>', [
        webapp2.Route('/', handler=FrontPage, name="www-front"),
        webapp2.Route('/register', handler=RegisterPage, name="www-register"),
        webapp2.Route('/dashboard', handler=DashboardPage, name="www-dashboard"),
        webapp2.Route('/logout', handler=Logout, name="www-logout"),
        webapp2.Route('/login', handler=LoginPage, name="www-login"),
        webapp2.Route('/fblogin', handler=FBLoginPage, name="www-fblogin"),
        webapp2.Route('/forgot-pass', handler=ForgotPassHandler, name="www-forgot-pass"),
        webapp2.Route('/campaign/kristine', handler=CampaignPage, name="www-campaign"),
        webapp2.Route('/campaign/create', handler=CreateCampaignPage, name="www-create-campaign"),
        webapp2.Route('/campaign/list', handler=ListCampaignPage, name="www-list-campaign"),
        webapp2.Route('/password/reset', handler=PasswordReset, name="www-reset-pass"),
        webapp2.Route('/profile/<profile_id>', handler=ProfilePage, name="www-profile"),
        webapp2.Route('/myprofile/<profile_id>', handler=MyProfilePage, name="www-myprofile"),

        webapp2.Route('/profile/upload/profile_photo', handler=ProfilePhotoUploadPage, name="www-profile-photo-upload"),
        webapp2.Route(r'/<:.*>', ErrorHandler)
    ])
])
